
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        testGroupObject();
//        testGroupList();
    }

    public static void testGroupObject() {
        GroupObject<String> gr = new GroupObject("Hello");
        gr.behaveLikeNamedList(true);
        System.out.println(gr);
        gr.add("Hio");
        gr.add("salad");
        System.out.println(gr);
        gr.behaveLikeNamedList(false);
        gr.add("salads");
        System.out.println(gr);
        gr.setToString(
                (GroupObject<String> gr1) -> (gr1.getIdentifiableElement() + " - SIZE:" + gr1.getElements().size())
        );
        System.out.println(gr);
        

    }

    // <editor-fold desc="Neighbors">
    public static void testNeighbors() {
        Integer[][] tempArr = new Integer[30][30];
        int[][] radial = new int[tempArr.length][tempArr[0].length];
        double r = 12;
        int x = tempArr[0].length / 2;
        int y = tempArr.length / 2;
        String[][] radArr = radiusCountS(tempArr, x, y, r, false);
        for (String[] ROW : radArr) {
            System.out.println(Arrays.toString(ROW));
        }
    }

    public static <T> int findNeighbors(T[][] arr, int x, int y, double r, boolean... countSelf) {
        int retVal = 0;
        for (int ROW = 0; ROW < arr.length; ROW++) {
            for (int COL = 0; COL < arr[ROW].length; COL++) {
                if (!(countSelf.length > 0 && countSelf[0]) && ROW == y && COL == x) {
                    continue;
                }
                double dx = COL - x;
                double dy = ROW - y;
                if (Math.sqrt(dx * dx + dy * dy) <= r) {
                    retVal++;
                }
            }
        }
        return retVal;
    }

    public static <T> String[][] radiusCount(T[][] arr, int x, int y, double r, boolean... countSelf) {
        String[][] retArr = new String[arr.length][arr[0].length];
        for (int ROW = 0; ROW < arr.length; ROW++) {
            for (int COL = 0; COL < arr[ROW].length; COL++) {
                if (!(countSelf.length > 0 && countSelf[0]) && ROW == y && COL == x) {
                    retArr[ROW][COL] = "0.00\t";
                    continue;
                }
                double dx = COL - x;
                double dy = ROW - y;
                double dist = Math.sqrt(dx * dx + dy * dy);
                if (dist <= r) {
                    retArr[ROW][COL] = Math.round(dist * 100) / 100.0 + "\t";
                } else {
                    retArr[ROW][COL] = "0.00\t";
                }
            }
        }
        return retArr;
    }

    public static <T> String[][] radiusCountS(T[][] arr, int x, int y, double r, boolean... countSelf) {
        String[][] retArr = new String[arr.length][arr[0].length];
        for (int ROW = 0; ROW < arr.length; ROW++) {
            for (int COL = 0; COL < arr[ROW].length; COL++) {
                if (!(countSelf.length > 0 && countSelf[0]) && ROW == y && COL == x) {
                    retArr[ROW][COL] = "0";
                    continue;
                }
                double dx = COL - x;
                double dy = ROW - y;
                double dist = Math.sqrt(dx * dx + dy * dy);
                if (dist <= r) {
                    retArr[ROW][COL] = "1";
                } else {
                    retArr[ROW][COL] = "0";
                }
            }
        }
        return retArr;
    }
    // </editor-fold>
}
